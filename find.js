cb = ( x ) => x === "valueToCompare" ? true : false;
function find(elements, cb) {
  let found = undefined;
  if(!elements || !Array.isArray(elements)) return [];
  for ( let i = 0; i < elements.length; ++i) {
    if ( cb(elements[i]) ) { return found = elements[i] }
  }
  return found;
}
// let something = [undefined, null,"a"];

module.exports = {
    find,
}


