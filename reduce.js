const items = [1, 2, 3, 4, 5, 5];
cb = (reduced, value) => reduced += value;

function reduce(elements, cb, startingValue) {
    // body...
    let index = startingValue || 0;
    const control = elements[index];
    let reduced = control;
    if(!elements || !Array.isArray(elements)) return [];
    for(let i = index+1; i < elements.length; ++i){
        reduced = cb(reduced, elements[i])
    }
    return reduced;
}

module.exports = reduce;




