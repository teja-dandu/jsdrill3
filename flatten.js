function flatten(elements) {
    // body...

    let toFlat = [];
    if(!elements || !Array.isArray(elements)) return [];
    for(let i = 0; i < elements.length; i++){
        if(Array.isArray(elements[i])){
            toFlat = toFlat.concat(flatten(elements[i]));
        }
        else{
            toFlat.push(elements[i]);
        }
    }
    return toFlat;
}
module.exports = {
    flatten,
};

