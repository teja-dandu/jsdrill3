cb = ( x ) => x*2;
function map(elements, cb) {
  let returnArray = [];
  if(!elements || !Array.isArray(elements)) return [];

  for ( let value of elements ){
    returnArray.push(cb(value));
  }
  return returnArray;
}
// console.log(map(items, cb));
module.exports = {
    map,
};

