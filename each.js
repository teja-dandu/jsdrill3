const items = [1, 2, 3, 4, 5, 5];
let cb = () => "";
cb = (x) => x;

cb = (x) => console.log(x-1);

function each(elements, cb) {
    // body...
    if(!elements || !Array.isArray(elements)) return [];
    for(let value of elements){
        cb(value);
    }
}
each(items, cb);

module.exports = {
    each,
}

